package com.aparna.restaurantapp.activities.profile;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.Utilities.Validations;
import com.aparna.restaurantapp.databinding.ActivityProfileBinding;


public class ProfileViewModel extends AndroidViewModel {
    private ProfileRepository repository;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        repository = ProfileRepository.getInstance(application);
    }

    public MutableLiveData<String> getProfile(RestAPIClientHelper restAPIClientHelper) {
        return repository.getApiResponse(restAPIClientHelper);
    }
    public MutableLiveData<String> updateProfile(RestAPIClientHelper restAPIClientHelper) {
        return repository.updateProfile(restAPIClientHelper);
    }



    public boolean validate(Context context, ActivityProfileBinding profileBinding) {
        if (profileBinding.edtSalutation.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.salutation_validate));
            return false;
        }else if (profileBinding.fName.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.fname_validate));
            return false;
        } else if (profileBinding.lName.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.lname_validate));
            return false;
        } else if (profileBinding.edtMNo.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.mb_validate));
            return false;
        }else if (profileBinding.email.getText().toString().equalsIgnoreCase("") || Validations.isEnailValid(profileBinding.email.getText().toString())){
            Utils.getToast(context,context.getResources().getString(R.string.enter_email_validate));
            return  false;
        }
        return true;
    }
}
