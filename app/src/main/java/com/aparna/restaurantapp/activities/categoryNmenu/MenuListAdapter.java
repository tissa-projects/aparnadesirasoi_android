package com.aparna.restaurantapp.activities.categoryNmenu;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.Utilities.ItemAnimation;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.Utilities.ViewAnimation;
import com.aparna.restaurantapp.databinding.RecyclerMenuListBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private JSONArray jsonArray;
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private int animation_type = 0;

    private ArrayList<JSONObject> productList;
    private static final String TAG = MenuListAdapter.class.getSimpleName();


    public interface OnItemClickListener {
        void onItemClick(JSONObject obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public MenuListAdapter(Context context, int animation_type) {
        ctx = context;
        jsonArray = new JSONArray();
        productList = new ArrayList<>();
        this.animation_type = animation_type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerMenuListBinding recyclerCategoryListBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_menu_list, parent, false);

        return new MyViewHolder(recyclerCategoryListBinding);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                try {
                    if (holder instanceof MyViewHolder) {
                        MyViewHolder myViewHolder = (MyViewHolder) holder;
                        String imgUrl = "";
                        JSONObject jsonObject = productList.get(position);
                        Log.e(TAG, "onBindViewHolder: "+jsonObject );
                        if (jsonObject.has("product_name")) {
                            myViewHolder.recyclerMenuListBinding.menuName.setText(jsonObject.getString("product_name"));
                        } else {
                            myViewHolder.recyclerMenuListBinding.menuName.setText(jsonObject.getString("productName"));
                        }
                        if (jsonObject.has("product_url")) {
                            imgUrl=jsonObject.getString("product_url");
                        } else {
                            imgUrl = jsonObject.getString("productUrl");
                        }
                        if ((!imgUrl.equalsIgnoreCase("")) || (imgUrl != null) ) {
                            Utils.displayImageOriginalString(ctx, myViewHolder.recyclerMenuListBinding.image, imgUrl);
                        }
                        myViewHolder.recyclerMenuListBinding.price.setText(SharePreferenceUtil.getSPstringValue(ctx,
                                SharePreferenceUtil.CURRENCY_TYPE)+jsonObject.getString("price"));

                        myViewHolder.recyclerMenuListBinding.mlParent.setOnClickListener((v)-> {
                            mOnItemClickListener.onItemClick(jsonObject, position);

                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            setAnimation(holder.itemView, position);

    }

    private boolean toggleLayoutExpand(boolean show, View view, View lyt_expand) {
        Utils.toggleArrow(show, view);
        if (show) {
            ViewAnimation.expand(lyt_expand);
        } else {
            ViewAnimation.collapse(lyt_expand);
        }
        return show;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                on_attach = false;
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    /*public void setData(JSONArray jsonArray){
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }*/


    public void setData(JSONArray jsonArray) {

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                productList.add(jsonArray.getJSONObject(i));
            }
            Log.e(TAG, "setData: "+productList.toString() );
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void clearProductLiist(){
        productList = null;
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        cookieManager.removeAllCookie();
        productList = new ArrayList<>();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerMenuListBinding recyclerMenuListBinding;

        public MyViewHolder(@NonNull RecyclerMenuListBinding recyclerMenuListBinding) {
            super(recyclerMenuListBinding.getRoot());
            this.recyclerMenuListBinding = recyclerMenuListBinding;
        }
    }


    private int lastPosition = -1;
    private boolean on_attach = true;

    private void setAnimation(View view, int position) {
        if (position > lastPosition) {
            ItemAnimation.animate(view, on_attach ? position : -1, animation_type);
            lastPosition = position;
        }
    }
}
