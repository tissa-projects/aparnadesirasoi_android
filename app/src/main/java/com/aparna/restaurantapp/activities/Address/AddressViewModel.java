package com.aparna.restaurantapp.activities.Address;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.activities.ServiceCall.RestAsyncTask;
import com.aparna.restaurantapp.databinding.ActivityBillingAddressBinding;


public class AddressViewModel extends AndroidViewModel {
    private AddressRepository repository;

    public AddressViewModel(@NonNull Application application) {
        super(application);

        repository = AddressRepository.getInstance(application);
    }

    public MutableLiveData<String> getBillingAddress(RestAPIClientHelper helper) {
        return repository.getBillingAddress(helper);
    }

    public MutableLiveData<String> setBillingAddress(RestAPIClientHelper helper) {
        return repository.setBillingAddress(helper);
    }

    public MutableLiveData<String> getStateList(RestAPIClientHelper helper) {
        return repository.getStateList(helper);
    }

    public boolean biillingValidate(Context context, ActivityBillingAddressBinding addressBinding) {
        if (!addressBinding.checkboxCurrentAddress.isChecked()) {
            if (addressBinding.billingEdtName.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.name));
                return false;
            } else if (addressBinding.billingEdtAddressLine.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.address));
                return false;
            }
//            else if (addressBinding.billingEdtHouseNo.getText().toString().equalsIgnoreCase("")) {
//                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.house_no));
//                return false;
//            }
            else if (addressBinding.billingEdtZipCode.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.zip_postal));
                return false;
            } else if (addressBinding.billingEdtCity.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.city));
                return false;
            } else if (addressBinding.billingEdtState.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.state));
                return false;
            } else if (addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.country));
                return false;
            }
        }
        return true;
    }



    public MutableLiveData<String> addFee(RestAPIClientHelper restAPIClientHelper) {
        return repository.addFee(restAPIClientHelper);
    }

    public MutableLiveData<String> getShippingFees(RestAPIClientHelper restAPIClientHelper) {
        return repository.getShippingFees(restAPIClientHelper);
    }

    public MutableLiveData<String> setOrder(RestAPIClientHelper helper) {
        return repository.setOrder(helper);
    }

    public MutableLiveData<String> getProfile(RestAPIClientHelper restAPIClientHelper) {
        return repository.getApiResponse(restAPIClientHelper);
    }
    public MutableLiveData<String> payment(RestAPIClientHelper helper) {
        return repository.payment(helper);
    }
}
