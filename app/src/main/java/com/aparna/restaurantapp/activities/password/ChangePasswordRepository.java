package com.aparna.restaurantapp.activities.password;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.activities.ServiceCall.RestAsyncTask;

public class ChangePasswordRepository {
    private static Application application;
    private static ChangePasswordRepository instance;
    private static final String TAG = ChangePasswordRepository.class.getSimpleName();


    public static ChangePasswordRepository getInstance(Application applicationContext) {
        application = applicationContext;
        if (instance == null) {
            instance = new ChangePasswordRepository();

        }
        return instance;
    }

    public MutableLiveData<String> changePassoword(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
