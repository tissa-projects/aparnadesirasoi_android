package com.aparna.restaurantapp.activities.categoryNmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.databinding.RecyclerProductIngredientBinding;
import com.aparna.restaurantapp.models.SelectedIngredentModel;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

public class IngredientAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private JSONArray jsonArray;
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private OnvalueChangeListener onvalueChangeListener;
    private Map<Integer, SelectedIngredentModel> selectedIngredient;
    public static final String TAG = IngredientAdapter.class.getSimpleName();


    public interface OnItemClickListener {
        void onItemClick(JSONObject obj, int position, boolean ischecked, String qty, double price, IngredientAdapter.MyViewHolder myViewHolder);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public interface OnvalueChangeListener {
        void onItemClick(MyViewHolder view1, String price, CheckBox checkBox, int oldValue, int newValue,int position);
    }

    public void setOnIngredientQtyListener(final OnvalueChangeListener onvalueChangeListener) {
        this.onvalueChangeListener = onvalueChangeListener;
    }

    public IngredientAdapter(Context context) {
        ctx = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerProductIngredientBinding recyclerProductIngredientBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_product_ingredient, parent, false);

        return new MyViewHolder(recyclerProductIngredientBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            try {
                MyViewHolder view = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                if (selectedIngredient.containsKey(position))
                    view.recyclerProductIngredientBinding.checkboxIngredient.setChecked(selectedIngredient.get(position).isChecked());
                view.recyclerProductIngredientBinding.txtIngredient.setText(jsonObject.getString("ingredient_name"));
                view.recyclerProductIngredientBinding.price.setText(SharePreferenceUtil.getSPstringValue(ctx, SharePreferenceUtil.CURRENCY_TYPE) + jsonObject.getString("price"));
                view.recyclerProductIngredientBinding.checkboxIngredient.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mOnItemClickListener.onItemClick(jsonObject, position, isChecked, view.recyclerProductIngredientBinding.txtQty.getNumber(),
                                Double.valueOf(view.recyclerProductIngredientBinding.price.getText().toString().replace("$", "")),
                                view);
                    }
                });

                view.recyclerProductIngredientBinding.txtQty.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
                    @Override
                    public void onValueChange(ElegantNumberButton view1, int oldValue, int newValue) {
                        try {
                            onvalueChangeListener.onItemClick(view, jsonObject.getString("price"),
                                    view.recyclerProductIngredientBinding.checkboxIngredient, oldValue, newValue,position);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*private double calculateSubTotalOverQty(MyViewHolder view1, String price, CheckBox checkBox) {
        view1.recyclerProductIngredientBinding.txtQty.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Log.d(TAG, String.format("oldValue: %d   newValue: %d", oldValue, newValue));
                onvalueChangeListener.onItemClick(view1, price, checkBox, oldValue, newValue);
            }
        });
        return Double.valueOf(view1.recyclerProductIngredientBinding.price.getText().toString().replace("$", ""));
    }*/

    public int getItemCount() {
        return jsonArray.length();
    }

    public void setData(JSONArray jsonArray, Map<Integer, SelectedIngredentModel> selectedIngredient) {
        this.jsonArray = jsonArray;
        this.selectedIngredient = selectedIngredient;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerProductIngredientBinding recyclerProductIngredientBinding;

        public MyViewHolder(@NonNull RecyclerProductIngredientBinding recyclerProductIngredientBinding) {
            super(recyclerProductIngredientBinding.getRoot());
            this.recyclerProductIngredientBinding = recyclerProductIngredientBinding;
        }
    }
}
