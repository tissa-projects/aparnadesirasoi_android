package com.aparna.restaurantapp.activities.categoryNmenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.Utilities.CustomDialogs;
import com.aparna.restaurantapp.Utilities.ItemAnimation;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.Utilities.UserSession;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.Utilities.VU;

import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.activities.cart.CartActivity;
import com.aparna.restaurantapp.databinding.ActivityMenuListBinding;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MenuListActivity extends AppCompatActivity {

    private ActivityMenuListBinding binding;
    private MenuViewModel catalogViewModel;
    private MenuListAdapter menuListAdapter;
    private int animation_type = ItemAnimation.FADE_IN;
    private Context context;
    private JSONObject jsonObject;
    private String categoryId = "";
    // private Set<String> catalogFilter;
   /*
    //variables for pagination
    private ProgressBar progressBar;
    private int page = 1;   // first page for product list
    private boolean isLoading = true, recyclerSearchType = false;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private int view_threshold = 6;*/
    private Bundle bundle = null;
    public static final String TAG = MenuListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_menu_list);
        catalogViewModel = ViewModelProviders.of(this).get(MenuViewModel.class);
        context = MenuListActivity.this;
       /* catalogFilter = new HashSet<>();
        catalogFilter.add("All");*/
        getIntentData();
        initRecycler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void getIntentData() {
        try {
            bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("jsonData");
                jsonObject = new JSONObject(data);
                Log.e(TAG, "getIntentData: " + jsonObject);
                binding.txtStoreName.setText(jsonObject.getString("category"));
                categoryId = jsonObject.getString("category_id");
                Log.e(TAG, "getIntentData: storeId: " + categoryId);


                if (VU.isConnectingToInternet(context)) {
                    getCart();
                }

            }

            binding.rlCart.setOnClickListener(v -> {
                startActivity(new Intent(context, CartActivity.class));
                Utils.fadeAnimation(context);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        if (VU.isConnectingToInternet(context)) {
            getMenuList();
            if (!SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID).equalsIgnoreCase("")) {
                cartCount();  //cart item count
            }
        }

        binding.searchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do your search
                menuListAdapter.clearProductLiist();
                searchProduct();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                  /*  recyclerSearchType = false;
                    page = 1;
                    catalogAdapter.clearProductLiist();
                    getCatalogList();*/
                }
                return false;
            }
        });
}


    private void initRecycler() {
        menuListAdapter = new MenuListAdapter(context, animation_type);
        binding.setMenuAdapter(menuListAdapter);
        // on item list clicked
        menuListAdapter.setOnItemClickListener((JSONObject obj, int position) -> {
            startActivity(new Intent(context, MenuDetailsActivity.class).putExtra("data", obj.toString()));
            Utils.fadeAnimation(context);
        });
    }

    private void getMenuList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            //helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.menu_list) + categoryId);
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.menu_list_all));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        catalogViewModel.getMenuList(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        Log.e(TAG, "getMenuList: " + jsonArray);
                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, getResources().getString(R.string.no_catagories));
                        }
                        menuListAdapter.setData(jsonArray);

                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

    // get cart
    private void getCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*if (page == 1) {
            dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        }*/
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        catalogViewModel.getCart(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "getCart: " + jsonObject);
                        JSONArray resultArray = jsonObject.getJSONArray("results");
                        if (resultArray.length() == 0) {
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, "");
                            createCart();
                        }else {
                            JSONObject cartObj = resultArray.getJSONObject(0);
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, cartObj.getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, "restaurant");
                        }
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

    //create cart
    private void createCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        JSONObject reqObj = new JSONObject();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.create_cart_api);
        try {
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("restaurant", getResources().getString(R.string.restaurant));
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        catalogViewModel.createCart(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "createCart : " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 201) {
                        JSONObject resultObj = jsonObject;
                        if (resultObj != null) {
                            binding.cartcount.setText("0");
                            Log.e(TAG, "createCart:cart id " + resultObj.getString("id"));
                            Log.e(TAG, "createCart:cart id " + resultObj);
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, resultObj.getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, resultObj.getString("restaurant"));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    //cart item count
    private void cartCount() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_item) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "&status=ACTIVE");
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        catalogViewModel.getCartCount(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "cartCount: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        if (jsonObject.length() > 0) {
                            JSONArray resultArray = jsonObject.getJSONArray("results");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, String.valueOf(resultArray.length()));
                            binding.cartcount.setText(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));
                        } else {
                            binding.cartcount.setText("0");
                        }

                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.delete_cart_api) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "/";
        try {
            helper.setContentType("application/json");
            helper.setMethodType(context.getResources().getString(R.string.DELETE));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        catalogViewModel.deleteCart(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCart: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 204) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                        createCart();
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void searchProduct() {
        String productName = binding.searchBox.getQuery().toString();

        String query = "query { productSearch(token :\""+getResources().getString(R.string.graphql_token)+
                "\", productName:\"" + productName +
                "\", restaurantId:\"" + getResources().getString(R.string.restaurant) +
                "\",categoryId:" + Integer.valueOf(categoryId) + ") { productId productName productCategory productUrl price taxExempt categoryId} }";
        JSONObject jsonObject3 = new JSONObject();
        try {
            jsonObject3.put("query", query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "graphql/");
            helper.setUrlParameter(jsonObject3.toString());
            helper.setAction(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        catalogViewModel.getSearchData(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("response_code") == 200) {
                        Log.e(TAG, "graphQlEx: " + jsonObject.toString());
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONObject("data").getJSONArray("productSearch");
                        if (jsonArray.length() > 0) {
                            menuListAdapter.setData(jsonArray);
                        } else {

                            menuListAdapter.setData(new JSONArray());  // biank array for  no prooduct
                            Utils.getToast(context, getResources().getString(R.string.no_product_for_search));
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.fadeAnimation(context);
    }
}
