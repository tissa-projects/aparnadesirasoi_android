package com.aparna.restaurantapp.activities.Address;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.Utilities.CustomDialogs;
import com.aparna.restaurantapp.Utilities.Preferences;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.Utilities.UserSession;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.Utilities.VU;
import com.aparna.restaurantapp.Utilities.swa.RetrofitClient;
import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.activities.payment.BackToStoreActivity;
import com.aparna.restaurantapp.databinding.ActivityBillingAddressBinding;
import com.aparna.restaurantapp.models.allmodel.CustomerDetailRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillingAddressActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ActivityBillingAddressBinding addressBinding;
    private AddressViewModel viewModel;
    private Context context;
    private NavController navController;
    // private JSONObject customerObj = null;
    private JSONArray cartDataArray = null;
    private JSONObject bundle = null, jsonObjectAddress;
    private String name, address1, houseNo, zip, city, state, country, shippingId = "0";
    ;
    private boolean isBillAddrAvail = false;
    private String strBillingAddressId = null, strCounntryShortCode = "US", subTotal = "0";
    private ArrayList<String> stateNameList = null;
    public static final String TAG = BillingAddressActivity.class.getSimpleName();

    private String currencyType = "$";

    private JSONObject addressObj = null;
    private String tip = "0", customTip = "0", shippingMethdName, noTaxTotal = "0";

    // private PaymentViewModel paymentViewModel;
    String orderid;
    String subtotal,
            total,
            extra,
            tax, discount;
    String receipt_email,
            phone_no;
    JSONObject orderdetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_billing_address);
        addressBinding = DataBindingUtil.setContentView(this, R.layout.activity_billing_address);
        viewModel = ViewModelProviders.of(this).get(AddressViewModel.class);
        //  paymentViewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        context = BillingAddressActivity.this;
        getIntentData();
    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {

                String data1 = bundle.getString("cartDataArray");
                subTotal = bundle.getString("subTotal");
                cartDataArray = new JSONArray(data1);
                Log.e(TAG, "getIntentData: " + cartDataArray.toString());

//                JSONObject bundleData = new JSONObject(data);
//                String strAddress = bundleData.getString("addressObj");
//                addressObj = new JSONObject(strAddress);
//                Log.e(TAG, "onCreate: addressObj: " + addressObj.toString());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        init();
        addressBinding.billingEdtName.setText("");
        addressBinding.billingEdtAddressLine.setText("");
        addressBinding.billingEdtHouseNo.setText("");
        addressBinding.billingEdtZipCode.setText("");
        addressBinding.billingEdtCity.setText("");
        addressBinding.billingEdtState.setText("");
        addressBinding.billingEdtCountry.setText("");
        getLocation();
    }

    public void getLocation() {

        try {
            addressBinding.billingEdtCountry.setText("United States of America");
            strCounntryShortCode = "US";
            if (!addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                getStateList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Shipping Address");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        if (VU.isConnectingToInternet(context)) {
            getBillingAddress();
//            if(Preferences.getUserProfile().equals("1")){
//                addFees();
//            }


            getProfile();
            getShippingMethods();

            //getCountryList();

        }
        addressBinding.billingEdtCountry.setOnClickListener(this::onClick);
        // addressBinding.shippingEdtCountry.setOnClickListener(this::onClick);
        // addressBinding.shippingEdtState.setOnClickListener(this::onClick);
        addressBinding.billingEdtState.setOnClickListener(this::onClick);
        addressBinding.refresh.setOnRefreshListener(this);
        addressBinding.checkboxCurrentAddress.setOnClickListener(this);

        addressBinding.btnNext.setOnClickListener(v -> {    // ===================next btn click============================
            bundle = new JSONObject();
            try {


                setAddressData();
                JSONObject billingAddressObj = new JSONObject();
                Log.e(TAG, "init: " + addressBinding.checkboxCurrentAddress.isChecked());

                Log.e(TAG, "init: " + name + " " + " " + address1 + " " + houseNo + " " + zip + " " + city + " " + state);
                billingAddressObj.put("name", name);
                billingAddressObj.put("address", address1);
                billingAddressObj.put("house_number", houseNo);
                billingAddressObj.put("zip", zip);
                billingAddressObj.put("city", city);
                billingAddressObj.put("country", country);
                billingAddressObj.put("state", state);


                bundle.put("addressObj", billingAddressObj.toString());
                bundle.put("counntryShortCode", strCounntryShortCode);
                if (VU.isConnectingToInternet(context)) {

                    if (viewModel.biillingValidate(context, addressBinding)) {
                        if (isBillAddrAvail) {
                            updateBillingAddress();
                        } else {
                            setBillingAddress();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        custumer();
    }
/*

    //country dialog
    private void setCountry() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Country");
        // add a list
        builder.setItems(Utils.GetStringArray(countryNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                addressBinding.billingEdtCountry.setText(countryNameList.get(position));
                addressBinding.billingEdtState.setText("");
                strCounntryShortCode = counntryShortCodeList.get(position);
                dialog.dismiss();
                if (countryNameList.get(position).equalsIgnoreCase(addressBinding.billingEdtCountry.getText().toString())) {
                    getStateList();
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }
*/

    //state dialog
    private void setstateDialog(String type) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select State");
        // add a list
        builder.setItems(Utils.GetStringArray(stateNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                addressBinding.billingEdtState.setText(stateNameList.get(position));
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void setBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_billing_address_api);
        try {

            reqObj.put("name", name);
            reqObj.put("company_name", "");
            reqObj.put("address", address1);
            reqObj.put("house_number", houseNo);
            reqObj.put("zip", zip);
            reqObj.put("city", city);
            reqObj.put("country", strCounntryShortCode);
            reqObj.put("state", state);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.set_billing_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.setBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setBillingAddress: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 201) {
                        JSONObject resultObj = jsonObject;
                        Log.e(TAG, "setBillingAddress: resultObj: " + resultObj);

//                        Intent intent = new Intent(context, PaymentActivity.class);
//                        intent.putExtra("bundle", bundle.toString())
//                                .putExtra("cartDataArray", cartDataArray.toString())
//                                .putExtra("subTotal", subTotal);
//                        startActivity(intent);
//                        if (Preferences.getUserProfile().equals("2")) {
//                            addFees();
//                        }
                        jsonObjectAddress = resultObj;
                        addFees();
                        //  .putExtra("noTaxTotal", noTaxTotal)
                        // .putExtra("shippingId", shippingId)
                        //   .putExtra("shippingMethdName", shippingMethodName);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_billing_address_api) + strBillingAddressId + "/";
        try {
            Log.e(TAG, "updateBillingAddress: " + name + " " + " " + address1 + " " + houseNo + " " + zip + " " + city + " " + state);
            reqObj.put("name", name);
            reqObj.put("company_name", "");
            reqObj.put("address", address1);
            reqObj.put("house_number", houseNo);
            reqObj.put("zip", zip);
            reqObj.put("city", city);
            reqObj.put("country", strCounntryShortCode);
            reqObj.put("state", state);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.set_billing_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.setBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateBillingAddress: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject resultObj = jsonObject;
                        Log.e(TAG, "updateBillingAddress: resultObj: " + resultObj);
//                        Intent intent = new Intent(context, PaymentActivity.class);
//                        intent.putExtra("bundle", bundle.toString())
//                                .putExtra("cartDataArray", cartDataArray.toString())
//                                .putExtra("subTotal", subTotal);
//                        startActivity(intent);
                        jsonObjectAddress = resultObj;
                        addFees();
//                        if (Preferences.getUserProfile().equals("2")) {
//
//                        }else {
//
//                        }
                        //  setOrderDetails();
                    } else {
                        getResources().getString(R.string.no_response);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void getBillingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shipping_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getBillingAddress: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("results");

                        if (resultArray.length() > 0) {
                            addressBinding.llCurrentAddress.setVisibility(View.VISIBLE);
                            addressBinding.txtNote.setVisibility(View.VISIBLE);
                            addressBinding.checkboxCurrentAddress.setChecked(true);
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            Log.e(TAG, "getBillingAddress: resultObj: " + resultObj.toString());
                            jsonObjectAddress = resultObj;
                            name = resultObj.getString("name");
                            address1 = resultObj.getString("address");
                            houseNo = resultObj.getString("house_number");
                            zip = resultObj.getString("zip");
                            city = resultObj.getString("city");
                            state = resultObj.getString("state");
                            country = resultObj.getString("country");
                            addressBinding.edtCurrentAddress.setText(name + ",\n" + address1 + ", " + houseNo + ",\n" + city + ", " + state + ", " + country + ",\n" + zip);


                            addressBinding.billingEdtName.setEnabled(false);
                            addressBinding.billingEdtAddressLine.setEnabled(false);
                            addressBinding.billingEdtHouseNo.setEnabled(false);
                            addressBinding.billingEdtCity.setEnabled(false);
                            addressBinding.billingEdtZipCode.setEnabled(false);
                            addressBinding.billingEdtState.setEnabled(false);

//                            addressBinding.billingEdtName.setClickable(false);
//                            addressBinding.billingEdtAddressLine.setClickable(false);
//                            addressBinding.billingEdtHouseNo.setClickable(false);
//                            addressBinding.billingEdtCity.setClickable(false);
//                            addressBinding.billingEdtZipCode.setClickable(false);
//                            addressBinding.billingEdtState.setClickable(false);
                           /* addressBinding.billingEdtName.setText(resultObj.getString("name"));
                            addressBinding.billingEdtCompanyName.setText(resultObj.getString("company_name"));
                            addressBinding.billingEdtAddressLine.setText(resultObj.getString("address"));
                            addressBinding.billingEdtHouseNo.setText(resultObj.getString("house_number"));
                            addressBinding.billingEdtZipCode.setText(resultObj.getString("zip"));
                            addressBinding.billingEdtCity.setText(resultObj.getString("city"));
                            addressBinding.billingEdtState.setText(resultObj.getString("state"));
                            addressBinding.billingEdtCountry.setText(resultObj.getString("country"));*/

                            strBillingAddressId = resultObj.getString("id");
                            isBillAddrAvail = true;

                        } else {
                            addressBinding.checkboxCurrentAddress.setChecked(false);

                            addressBinding.billingEdtName.setEnabled(true);
                            addressBinding.billingEdtAddressLine.setEnabled(true);
                            addressBinding.billingEdtHouseNo.setEnabled(true);
                            addressBinding.billingEdtCity.setEnabled(true);
                            addressBinding.billingEdtZipCode.setEnabled(true);
                            addressBinding.billingEdtState.setEnabled(true);

//                            addressBinding.billingEdtName.setClickable(true);
//                            addressBinding.billingEdtAddressLine.setClickable(true);
//                            addressBinding.billingEdtHouseNo.setClickable(true);
//                            addressBinding.billingEdtCity.setClickable(true);
//                            addressBinding.billingEdtZipCode.setClickable(true);
//                            addressBinding.billingEdtState.setClickable(true);
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }



    /*private void getCountryList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_country_api);
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCountry = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCountryList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogCountry.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCountryList : " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("data");

                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                countryNameList.add(resultArray.getJSONObject(i).getString("country"));
                                counntryShortCodeList.add(resultArray.getJSONObject(i).getString("country_code"));
                            }

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }*/

    private void getStateList() {
        stateNameList = new ArrayList<>();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_state_api) + "US";
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.get_state_api));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  dialogState = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getStateList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                //     dialogState.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStateList : " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("results");

                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                stateNameList.add(resultArray.getJSONObject(i).getString("state"));
                            }

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.billing_edt_country:
                //       setCountry();
                break;

            case R.id.billing_edt_state:
                if (!addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                    setstateDialog("billing");
                } else {
                    Utils.getToast(context, "First select country");
                }
                break;

            case R.id.checkbox_current_address:

                if (addressBinding.checkboxCurrentAddress.isChecked()) {
                    try {
                        name = jsonObjectAddress.getString("name");
                        address1 = jsonObjectAddress.getString("address");
                        houseNo = jsonObjectAddress.getString("house_number");
                        zip = jsonObjectAddress.getString("zip");
                        city = jsonObjectAddress.getString("city");
                        state = jsonObjectAddress.getString("state");
                        country = jsonObjectAddress.getString("country");

                        addressBinding.billingEdtName.setEnabled(false);
                        addressBinding.billingEdtAddressLine.setEnabled(false);
                        addressBinding.billingEdtHouseNo.setEnabled(false);
                        addressBinding.billingEdtCity.setEnabled(false);
                        addressBinding.billingEdtZipCode.setEnabled(false);
                        addressBinding.billingEdtState.setEnabled(false);

//                        addressBinding.billingEdtName.setClickable(true);
//                        addressBinding.billingEdtAddressLine.setClickable(true);
//                        addressBinding.billingEdtHouseNo.setClickable(true);
//                        addressBinding.billingEdtCity.setClickable(true);
//                        addressBinding.billingEdtZipCode.setClickable(true);
//                        addressBinding.billingEdtState.setClickable(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    setAddressData();
                }
                break;
        }
    }

    private void setAddressData() {
        if (addressBinding.checkboxCurrentAddress.isChecked()) {
            try {
                name = jsonObjectAddress.getString("name");
                address1 = jsonObjectAddress.getString("address");
                houseNo = jsonObjectAddress.getString("house_number");
                zip = jsonObjectAddress.getString("zip");
                city = jsonObjectAddress.getString("city");
                state = jsonObjectAddress.getString("state");
                country = jsonObjectAddress.getString("country");

                addressBinding.billingEdtName.setEnabled(false);
                addressBinding.billingEdtAddressLine.setEnabled(false);
                addressBinding.billingEdtHouseNo.setEnabled(false);
                addressBinding.billingEdtCity.setEnabled(false);
                addressBinding.billingEdtZipCode.setEnabled(false);
                addressBinding.billingEdtState.setEnabled(false);

//                addressBinding.billingEdtName.setClickable(true);
//                addressBinding.billingEdtAddressLine.setClickable(true);
//                addressBinding.billingEdtHouseNo.setClickable(true);
//                addressBinding.billingEdtCity.setClickable(true);
//                addressBinding.billingEdtZipCode.setClickable(true);
//                addressBinding.billingEdtState.setClickable(true);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e("x12","jhjhjj");
            addressBinding.billingEdtName.setEnabled(true);
            addressBinding.billingEdtAddressLine.setEnabled(true);
            addressBinding.billingEdtHouseNo.setEnabled(true);
            addressBinding.billingEdtCity.setEnabled(true);
            addressBinding.billingEdtZipCode.setEnabled(true);
            addressBinding.billingEdtState.setEnabled(true);





            name = addressBinding.billingEdtName.getText().toString();
            address1 = addressBinding.billingEdtAddressLine.getText().toString();
            houseNo = addressBinding.billingEdtHouseNo.getText().toString();
            zip = addressBinding.billingEdtZipCode.getText().toString();
            city = addressBinding.billingEdtCity.getText().toString();
            state = addressBinding.billingEdtState.getText().toString();
            country = strCounntryShortCode;



        }
    }


    @Override   // swipe refresh
    public void onRefresh() {
        if (VU.isConnectingToInternet(context)) {
            getBillingAddress();
            //getCountryList();
        }
        addressBinding.refresh.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }


    private void setOrderDetails() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_order_details_api);
        try {
            reqObj.put("status", "active");  //customer always be active
            reqObj.put("currency", "USD");
            reqObj.put("cart_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID));
            reqObj.put("subtotal", subTotal);
            reqObj.put("total", total);
            reqObj.put("extra", "");
            reqObj.put("customer", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("tip", tip);
            //   reqObj.put("service_fee", paymentBinding.txtServiceFee.getText().toString().substring(1));
            reqObj.put("service_fee", "0");
            reqObj.put("tax", tax);
            reqObj.put("discount", discount);
            reqObj.put("shipping_fee", 0);

            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.set_order_details_api));
        } catch (Exception e) {
            e.printStackTrace();

        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.setOrder(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setOrderDetails: " + jsonObject);
                    JSONObject dataObj = jsonObject;
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        //OrderDetails = dataObj;
                        Log.e(TAG, "setOrderDetails: " + dataObj.toString());
                        orderid = dataObj.getString("order_id");
                        orderdetail = dataObj;

                        payment();//order id
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        CustomDialogs.dialogShowMsg(context, dataObj.getString("msg"));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
            }
        });
    }


    private void addFees() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        //   String tip = (cartBinding.txtTip.getText().toString().equalsIgnoreCase("")) ? "0" : cartBinding.txtTip.getText().toString();
        try {
            JSONObject reqObj = new JSONObject();
            reqObj.put("sub_total", subTotal);
            reqObj.put("no_tax_total", 0);
            reqObj.put("restaurant_id", "1");
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            if (customTip.equalsIgnoreCase("0")) {
                reqObj.put("tip", tip);  // %
            } else {
                reqObj.put("custom_tip", customTip); // normal value
            }
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.add_fees));
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.add_fees));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.addFee(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "addFees: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {


                        subtotal = responseObj.getString("sub_total");
                        total = responseObj.getString("total");

                        tax = responseObj.getString("tax");
                        discount = responseObj.getString("discount");

                        setOrderDetails();
                        //  responseObj = responseObj.getJSONObject("data");
//                        paymentBinding.txtSubTotal.setText(currencyType + responseObj.getString("sub_total"));
//                        paymentBinding.txtVat.setText(currencyType + responseObj.getString("tax"));
//                        paymentBinding.txtTip.setText(currencyType + responseObj.getString("tip"));
//                        //   paymentBinding.txtServiceFee.setText(currencyType + responseObj.getString("service_fee"));
//                        paymentBinding.txtDiscount.setText(currencyType + responseObj.getString("discount"));
//                        paymentBinding.txtTotal.setText(currencyType + responseObj.getString("total"));
                        //  startActivity(new Intent(context, CheckOutActivity.class));
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        Utils.getToast(context, responseObj.getString("message"));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private void getProfile() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");

        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_customer_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getProfile(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "getProfile: " + responseObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        setProfileData(responseObject);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void setProfileData(JSONObject jsonObject) {
        try {
            JSONArray resultArray = jsonObject.getJSONArray("results");
            Log.e(TAG, "getCustomer: " + resultArray.toString());
            JSONObject resultObj = resultArray.getJSONObject(0);
            JSONObject customerObj = resultObj.getJSONObject("customer");
            String strMNo = null;
            if (resultObj.getString("phone_number").contains("+1")) {
                strMNo = resultObj.getString("phone_number").replace("+1", "");
            } else {
                strMNo = resultObj.getString("phone_number").replace("+91", "");
            }

            UserSession.createUserLoginSession(context, customerObj.getString("first_name"), customerObj.getString("last_name"),
                    resultObj.getString("salutation"), strMNo, customerObj.getString("email"),
                    customerObj.getString("username"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void custumer() {

        Call<CustomerDetailRequest> call = RetrofitClient.getInstance().getapi()
                .customerdetail("Token " + Preferences.getToken(), Preferences.getUserId());
        call.enqueue(new Callback<CustomerDetailRequest>() {
            @Override
            public void onResponse(Call<CustomerDetailRequest> call, Response<CustomerDetailRequest> response) {


                if (response.code() == 200) {
                    CustomerDetailRequest p = response.body();

                    receipt_email = p.getResults().get(0).getCustomer().getEmail();
                    phone_no = p.getResults().get(0).getPhoneNumber();

                } else if (response.code() == 401) {
                    // dialog.dismiss();
                    Toast.makeText(BillingAddressActivity.this, getResources().getString(R.string.credential_wrong), Toast.LENGTH_LONG).show();
                } else if (response.code() == 403) {
                    //  dialog.dismiss();
                    Toast.makeText(BillingAddressActivity.this, getResources().getString(R.string.email_not_verified), Toast.LENGTH_LONG).show();

                } else {
                    //   dialog.dismiss();
                    Toast.makeText(BillingAddressActivity.this, getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<CustomerDetailRequest> call, Throwable t) {
                /// dialog.dismiss();
            }
        });


    }


    private void payment() {

        RestAPIClientHelper helper = new RestAPIClientHelper();
        JSONObject reqObj = null;
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.payment_api);
        try {
            String currency = "USD";
            reqObj = new JSONObject();
            JSONObject cardObj = new JSONObject();
            JSONObject billingDetails = new JSONObject();
            JSONObject metadata = new JSONObject();
            JSONObject address = new JSONObject();
            Log.e(TAG, "payment: addressObj: " + shippingId);

            metadata.put("shippingmethod_id", shippingId).put("order_id", orderid)
                    .put("phone", phone_no)
                    .put("restaurant_id", getResources().getString(R.string.restaurant))
                    .put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            address.put("city", city).put("country", strCounntryShortCode)
                    .put("line1", address1).put("line2", "")
                    .put("postal_code", zip).put("state", state);
            billingDetails.put("address", address);
            cardObj.put("number", "4242424242424242")
                    .put("exp_month", "08")
                    .put("exp_year", "2025")
                    .put("cvc", "666");
            reqObj.put("amount", total)
                    .put("currency", currency).put("receipt_email", receipt_email)
                    .put("type", "card").put("card", cardObj)
                    .put("billing_details", billingDetails).put("metadata", metadata);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.payment_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.payment(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "payment: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject resultObj = jsonObject;
                        Log.e(TAG, "payment: " + resultObj.toString());
                        Utils.getToast(context, getResources().getString(R.string.order_placed));
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                        Intent intent = new Intent(context, BackToStoreActivity.class)
                                .putExtra("OrderDetails", orderdetail.toString())
                                .putExtra("addressDetails", jsonObjectAddress.toString());
                        startActivity(intent);
                        //showCartList(resultObj);   //for adding cart items to order item after payment success
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 400) {
                        Utils.getToast(context, getResources().getString(R.string.incorrect_cart_details));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                e.printStackTrace();
            }
        });
    }

    private void getShippingMethods() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shipping_methods) + "status=ACTIVE&restaurant=" + getResources().getString(R.string.restaurant));
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_shipping_methods));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getShippingFees(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getShippingMethods: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray jsonArray = responseObj.getJSONArray("results");
                        Log.e(TAG, "getShippingMethods: " + jsonArray);

                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, "No shipping method available");
                        } else {
                            shippingId = jsonArray.getJSONObject(0).getString("id");

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }
}
