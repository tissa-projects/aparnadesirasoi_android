package com.aparna.restaurantapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.activities.auth.LoginActivity;
import com.aparna.restaurantapp.activities.dashboard.DashBoardActivity;
import com.aparna.restaurantapp.Utilities.Preferences;
import com.aparna.restaurantapp.Utilities.UserSession;



public class SplashScreen extends AppCompatActivity {
    Context context;
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        context = SplashScreen.this;
        splashTime();
    }

    public void splashTime() {
        Preferences.appContext = getApplicationContext();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UserSession.isUserLoggedIn(context)){
                startActivity(new Intent(context, DashBoardActivity.class));
                }else {
                    startActivity(new Intent(context, LoginActivity.class));
                }
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
