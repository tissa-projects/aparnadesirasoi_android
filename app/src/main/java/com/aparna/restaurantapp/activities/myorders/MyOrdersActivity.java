package com.aparna.restaurantapp.activities.myorders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.activities.myorderdetails.MyOrderDetailsActivity;
import com.aparna.restaurantapp.Utilities.CustomDialogs;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.Utilities.UserSession;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.Utilities.VU;
import com.aparna.restaurantapp.databinding.ActivityMyOrdersBinding;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrdersActivity extends AppCompatActivity {

    private MyOrdersViewModel viewModel;
    private ActivityMyOrdersBinding myOrdersBinding;
    private Context context;
    private MyOrdersAdapter myOrdersAdapter;
    private View view;
    private static final String TAG = MyOrdersActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myOrdersBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_orders);
        viewModel = ViewModelProviders.of(this).get(MyOrdersViewModel.class);
        context = MyOrdersActivity.this;
        init();
        if (VU.isConnectingToInternet(context)) {
            getOrderList();
        }
    }

    private void init() {

        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("My Orders");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        viewModel = ViewModelProviders.of(this).get(MyOrdersViewModel.class);
        myOrdersBinding.refresh.setOnRefreshListener(this::onRefresh);
        myOrdersAdapter = new MyOrdersAdapter(context);
        myOrdersAdapter.ViewDetailsClick((position, jsonObject) -> {   // recycler View Details btn  click
            try {
                Intent intent = new Intent(context, MyOrderDetailsActivity.class);
                intent.putExtra("jsonData", jsonObject.toString());
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

//        myOrdersAdapter.cancelDeleteClick((position, jsonObject) -> {   // recycler View Details btn  click
//            if (VU.isConnectingToInternet(context))
//                dialogCancelOrder(jsonObject);  //just setting active order as cancel
//        });

        myOrdersBinding.setMyOrderAdapter(myOrdersAdapter);
    }

    //swife refersh for recycler
    private void onRefresh() {
        if (VU.isConnectingToInternet(context)) {
            getOrderList();
        }
        myOrdersBinding.refresh.setRefreshing(false);

    }


    public void dialogCancelOrder(JSONObject jsonObject) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delete);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.txt)).setText(getResources().getString(R.string.dialog_cancel_order_txt));

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (VU.isConnectingToInternet(context))
                    orderCancel(jsonObject);
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void getOrderList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_order_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_order_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getOrderList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONArray responseObj = new JSONArray(response);
                    Log.e(TAG, "getOrderList: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray resultArray = responseObj;
                        if (resultArray.length() > 0) {
                            myOrdersAdapter.setData(resultArray);
                        } else {
                            Utils.getToast(context, getResources().getString(R.string.no_order_available));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void orderCancel(JSONObject jsonObject) {
        RestAPIClientHelper helper = new RestAPIClientHelper();

        try {
            String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.cancel_order) + jsonObject.getJSONObject("order").getString("order_id");
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.DELETE));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_order_api));
            helper.setOrderId(jsonObject.getJSONObject("order").getString("order_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getOrderList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "orderCancel: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context, getResources().getString(R.string.order_cancl));
                        getOrderList();
                    } else {
                        CustomDialogs.dialogShowMsg(context, responseObj.getString("msg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
}