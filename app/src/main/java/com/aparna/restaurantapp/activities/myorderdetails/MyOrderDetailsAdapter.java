package com.aparna.restaurantapp.activities.myorderdetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.databinding.RecyclerMyOrderDetailsBinding;
import com.aparna.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.Utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrderDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = MyOrderDetailsAdapter.class.getSimpleName();
    private SetOnClickListener.setCardClick setViewDetails;
    private JSONArray jsonArray;


    public MyOrderDetailsAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerMyOrderDetailsBinding myOrdersDetailsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_my_order_details, parent, false);

        return new MyOrderDetailsAdapter.MyViewHolder(myOrdersDetailsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof MyViewHolder) {
                String strIngredient = "";
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                JSONArray ingredientarray = jsonObject.getJSONArray("ingredient");
                JSONObject productObj = jsonObject;
                String imgUrl = productObj.getString("product_url");
                Utils.displayImageOriginalString(context,myViewHolder.myOrderDetailsBinding.productImage,imgUrl);
                myViewHolder.myOrderDetailsBinding.productName.setText(productObj.getString("product_name"));
                myViewHolder.myOrderDetailsBinding.productQty.setText("Qty: "+jsonObject.getString("quantity"));
                String strprice = (String.valueOf(Double.valueOf(jsonObject.getString("quantity"))*Double.valueOf(productObj.getString("price"))));
                String currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
                myViewHolder.myOrderDetailsBinding.productTotal.setText("Price: "+currencyType+Utils.convertToUSDFormat(strprice));

                for (int i = 0;i<ingredientarray.length();i++){
                    strIngredient = strIngredient+ (i+1)+"."+ingredientarray.getJSONObject(i).getString("ingredient_name")+" : \n"+
                            "    Price :$"+ingredientarray.getJSONObject(i).getString("line_total")+" \n"+
                            "    Qty : "+ingredientarray.getJSONObject(i).getString("quantity")+"\n";
                }
                if (ingredientarray.length()>0){
                    myViewHolder.myOrderDetailsBinding.llIngredient.setVisibility(View.VISIBLE);
                }
                myViewHolder.myOrderDetailsBinding.txtIngredient.setText(strIngredient);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        //return 10;
        return jsonArray.length();
    }

    public void setData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerMyOrderDetailsBinding myOrderDetailsBinding;

        public MyViewHolder(@NonNull RecyclerMyOrderDetailsBinding myOrderDetailsBinding) {
            super(myOrderDetailsBinding.getRoot());
            this.myOrderDetailsBinding = myOrderDetailsBinding;
        }

       /* public void bind(Object obj) {
            myOrderDetailsBinding.setMyOrder((MyOrderModel) obj);
            myOrderDetailsBinding.executePendingBindings();
        }*/
    }

}
