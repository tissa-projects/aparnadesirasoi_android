package com.aparna.restaurantapp.activities.myorders;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.activities.ServiceCall.RestAsyncTask;

public class MyOrderRepository {

    private static Application application;
    private static MyOrderRepository instance;
    private static final String TAG = MyOrderRepository.class.getSimpleName();


    public static MyOrderRepository getInstance(Application applicationContext) {
        application = applicationContext;
        if (instance == null) {
            instance = new MyOrderRepository();

        }
        return instance;
    }

    public MutableLiveData<String> getOrderList(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

}
