package com.aparna.restaurantapp.activities.password;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.aparna.restaurantapp.R;
import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.aparna.restaurantapp.Utilities.CustomDialogs;
import com.aparna.restaurantapp.Utilities.SharePreferenceUtil;
import com.aparna.restaurantapp.Utilities.UserSession;
import com.aparna.restaurantapp.Utilities.Utils;
import com.aparna.restaurantapp.Utilities.VU;
import com.aparna.restaurantapp.databinding.ActivityChangePasswordBinding;

import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityChangePasswordBinding changePasswordBinding;
    private ChangePasswordViewModel changePasswordViewModel;
    private Context context;
    public static final String TAG = ChangePasswordActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changePasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        changePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        context = ChangePasswordActivity.this;
        init();;
    }


    private void init() {
        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Change Password");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        changePasswordBinding.btnUpdatePassword.setOnClickListener(this);
    }
    @Override  //change passwd click
    public void onClick(View v) {
        if (VU.isConnectingToInternet(context)){
            if (changePasswordViewModel.validate(context,changePasswordBinding)){
                updatePassword();
            }
        }
    }


    private void updatePassword() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_password);

        try {
            JSONObject req = new JSONObject();
            req.put("username", UserSession.getUserDetails(context).get("username"));
            req.put("old_password",changePasswordBinding.currentPassword.getText().toString());
            req.put("new_password1",changePasswordBinding.newPassword.getText().toString());
            req.put("new_password2",changePasswordBinding.confirmPassword.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.update_password));
            helper.setUrlParameter(req.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        changePasswordViewModel.changePassoword(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "getProfile: " + responseObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context,"Password changed  successfully");
                        finish();
                        Utils.fadeAnimation(context);
                    } else {
                        CustomDialogs.dialogShowMsg(context, responseObject.getJSONObject("data").getString("msg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.fadeAnimation(context);
    }
}
