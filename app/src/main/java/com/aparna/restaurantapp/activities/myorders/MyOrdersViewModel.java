package com.aparna.restaurantapp.activities.myorders;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.aparna.restaurantapp.activities.ServiceCall.RestAPIClientHelper;


public class MyOrdersViewModel extends AndroidViewModel {


    private MyOrderRepository repository;
    public MyOrdersViewModel(@NonNull Application application) {
        super(application);
        repository = MyOrderRepository.getInstance(application);
    }


    public MutableLiveData<String> getOrderList(RestAPIClientHelper helper) {
        return repository.getOrderList(helper);
    }



}