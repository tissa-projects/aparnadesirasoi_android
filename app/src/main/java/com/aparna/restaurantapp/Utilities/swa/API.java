package com.aparna.restaurantapp.Utilities.swa;


import com.aparna.restaurantapp.models.CartCountRequest;
import com.aparna.restaurantapp.models.RetrarentRequest;
import com.aparna.restaurantapp.models.allmodel.CustomerDetailRequest;
import com.aparna.restaurantapp.models.allmodel.LoginResponse;
import com.aparna.restaurantapp.models.allmodel.Loginpara;
import com.aparna.restaurantapp.models.allmodel.UserRestrout;
import com.aparna.restaurantapp.models.allmodel.UseridRequest;
import com.aparna.restaurantapp.models.allmodel.usermodel;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    @Headers("Content-Type: application/json")
    @GET("restaurant/101/")
    Call<RetrarentRequest> fetchdata(
            @Header("Authorization") String authorization

    );

    @POST("graphql/")
    Call<CartCountRequest> cartcount(
            @Body RequestBody query
    );

    @POST("/userrestaurant/")
    Call<UserRestrout> insertuser(
            @Header("Authorization") String authorization,
            @Body usermodel login
    );
    @Headers("Content-Type: application/json")
    @POST("rest-auth/login/v1/")
    Call<LoginResponse> login(
            @Body Loginpara login
    );
    @Headers("Content-Type: application/json")
    @GET("user/?")
    Call<UseridRequest> getuser(
            @Header("Authorization") String authorization,
            @Query("username") String status
    );

    @Headers("Content-Type: application/json")
    @GET("customer/?")
    Call<CustomerDetailRequest> customerdetail(
            @Header("Authorization") String authorization,
            @Query("customer_id") String customer_id
    );
}
