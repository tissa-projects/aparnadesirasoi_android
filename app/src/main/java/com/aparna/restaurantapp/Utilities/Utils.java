package com.aparna.restaurantapp.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.aparna.restaurantapp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import com.squareup.picasso.Picasso;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 12;

    public  static  void fadeAnimation(Context context){
        ((AppCompatActivity)context).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public static void getToast(final Context context, final String txt) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_custom, ((Activity) context).findViewById(R.id.custom_toast_container));
                TextView text = layout.findViewById(R.id.text);
                text.setText(txt);
                Toast toast = new Toast(context);
                toast.setGravity(Gravity.CENTER, 0, 40);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        });
    }

    public static String buildPostParameters(Object content) {
        String output = null;
        if ((content instanceof String) ||
                (content instanceof JSONObject) ||
                (content instanceof JSONArray)) {
            output = content.toString();
        } else if (content instanceof Map) {
            Uri.Builder builder = new Uri.Builder();
            HashMap hashMap = (HashMap) content;
            if (hashMap != null) {
                Iterator entries = hashMap.entrySet().iterator();
                while (entries.hasNext()) {
                    Map.Entry entry = (Map.Entry) entries.next();
                    builder.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
                    entries.remove(); // avoids a ConcurrentModificationException
                }
                output = builder.build().getEncodedQuery();
            }
        }

        return output;
    }

    public static String getBase64FromPath(String path) {
        String encodedString = "";
        try {
            File file = new File(path);
            encodedString = Base64.encodeToString(FileUtils.readFileToByteArray(file), Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "getBase64FromPath: " + encodedString);
        return encodedString;
    }

    public static Bitmap getBitmap(String image) {
        byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        return bitmap;
    }

    public static boolean toggleArrow(boolean show, View view) {
        return toggleArrow(show, view, true);
    }

    public static boolean toggleArrow(boolean show, View view, boolean delay) {
        if (show) {
            view.animate().setDuration(delay ? 200 : 0).rotation(180);
            return true;
        } else {
            view.animate().setDuration(delay ? 200 : 0).rotation(0);
            return false;
        }
    }

    public static String ConvertIntoDateFormat(String old, String newFormat, String strdate) {
        Log.e(TAG, "ConvertIntoDateFormat: "+strdate );
        String changeDate = null;
        try {
            DateFormat format = new SimpleDateFormat(old, Locale.ENGLISH);
            Date date = format.parse(strdate);
            SimpleDateFormat newformat = new SimpleDateFormat(newFormat, Locale.ENGLISH);
            String newDate = newformat.format(date);
            Log.e(TAG, "ConvertIntoDateFormat: "+newDate );
            changeDate = newDate.split(" ")[1] + " " + newDate.split(" ")[0] + ", " + newDate.split(" ")[2];
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return changeDate;
    }

    public static void displayImageOriginalString(Context ctx, final ImageView img, String url) {
        try {
            // Glide.with(ctx).load(url).load(R.drawable.logo).into(img);
            //  Picasso.get().load(url).placeholder(R.drawable.img_load100).into(img);
            Picasso.get().load(url).into(img);
        } catch (Exception e) {
            Log.e("Utilities : Tools", "displayImageOriginalString: " + e.getMessage());
        }
    }

    public static Drawable getDrawableFromURL(String imgUrl) {
        Drawable drawable =null;
        try {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        InputStream iStream = (InputStream) new URL(imgUrl).getContent();
        drawable = Drawable.createFromStream(iStream, "src name");
        }catch (Exception e){e.printStackTrace();}
        return drawable;
    }


    public static void displayImageRound(final Context ctx, final ImageView img, String url) {
        try {
            Glide.with(ctx).load(getDrawableFromURL(url)).asBitmap().centerCrop().into(new BitmapImageViewTarget(img) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(ctx.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img.setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception e) {
        }
    }

    public static Bitmap getImageBitmap(String url) {
        final Bitmap[] bm = {null};
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    URL aURL = new URL(url);
                    URLConnection conn = aURL.openConnection();
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);
                    bm[0] = BitmapFactory.decodeStream(bis);
                    bis.close();
                    is.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error getting bitmap", e);
                }
            }
        }).start();
        return bm[0];
    }

    public static boolean checkPermission(Context context) {
        return (ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    public static void requestPermission(Context context) {
        ActivityCompat.requestPermissions((Activity) context, new String[]{WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
    }

    // Function to convert ArrayList<String> to String[]
    public static String[] GetStringArray(ArrayList<String> arr) {
        // declaration and initialise String Array
        String str[] = new String[arr.size()];
        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {
            // Assign each value to String array
            str[j] = arr.get(j);
        }


        return str;
    }

    // Function to convert Set<String> to String[]
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String[] GetStringArray(Set<String> arr) {
        String[] array = arr.stream().toArray(String[]::new);

        return array;
    }

    public static String CurrentTimeStamp() {
        //Date object
        Date date = new Date();
        //getTime() returns current time in milliseconds
        long time = date.getTime();
        //Passed the milliseconds to constructor of Timestamp class
        Timestamp ts = new Timestamp(time);
        return ts.toString();
    }

    public static String convertToUSDFormat(String value) {
        Log.e(TAG, "convertToUSDFormat: value: " + value);
        DecimalFormat numberFormat = new DecimalFormat("######0.00");
        String convertedValue = numberFormat.format(Double.valueOf(value));
        return convertedValue;
    }

    public static String geLocationData(Context context, double latitude, double longitude, String type) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        String data = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses != null && !addresses.isEmpty()) {
                switch (type) {
                    case "country":
                        data = addresses.get(0).getCountryName();
                        break;
                    case "state":
                        data = addresses.get(0).getAdminArea();
                        break;
                    case "city":
                        data = addresses.get(0).getLocality();
                        break;
                    case "zipcode":
                        data = addresses.get(0).getPostalCode();
                        break;
                }
            }
            return data;
        } catch (IOException ignored) {
            //do something
        }
        return data;
    }



    /*
     int currentApiVersion = Build.VERSION.SDK_INT;

        if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (!Utils.checkPermission(context)) {
                Utils.requestPermission(context);
            }
        }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                Log.e("grantResult", grantResults[0] + "" + "" + PackageManager.PERMISSION_DENIED);
                if (grantResults.length > 0) {

                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean storageDenied = grantResults[0] == PackageManager.PERMISSION_DENIED;

                    if (storageDenied) {
                        Utils.requestPermission(context);
                    } else if (storageAccepted) {
                        //  Toast.makeText(getApplicationContext(), "Permission Granted, Now You Can Access Storage", Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }
    }*/
}
