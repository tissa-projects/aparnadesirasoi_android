package com.aparna.restaurantapp.Utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validations {

    public static final String EMAIL_PATTERN = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+";
    public static Pattern pattern;
    static Matcher matcher;

    public static boolean isEnailValid(String email) {
        // method to check edit text is fill or no
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email.trim());
        if (matcher.matches()) {
            return false;
        }
        return true;
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isFieldNotEmpty(String field) {
        boolean isNotempty = field.equalsIgnoreCase("") ? false : true;
        return isNotempty;
    }

}
