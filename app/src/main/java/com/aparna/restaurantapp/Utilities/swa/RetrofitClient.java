package com.aparna.restaurantapp.Utilities.swa;

import com.readystatesoftware.chuck.ChuckInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String BASE_URL = "https://restaurant60-be-dev-xtpocjmkpa-uw.a.run.app/";



    private static RetrofitClient mInstance;
    private Retrofit retrofit;
    private RetrofitClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new ChuckInterceptor(MyApp.getContext()));
        OkHttpClient build = builder.build();


        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(build)

                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();

        }
        return mInstance;
    }

    public API getapi() {
        return retrofit.create(API.class);
    }
}
