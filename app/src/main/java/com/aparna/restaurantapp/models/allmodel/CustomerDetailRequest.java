package com.aparna.restaurantapp.models.allmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CustomerDetailRequest {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("next")
    @Expose
    private Object next;
    @SerializedName("previous")
    @Expose
    private Object previous;
    @SerializedName("results")
    @Expose
 //   private CustomerDetailResponse[] results = null;
    private ArrayList<CustomerDetailResponse> results = null;
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public ArrayList<CustomerDetailResponse> getResults() {
        return results;
    }

    public void setResults(ArrayList<CustomerDetailResponse> results) {
        this.results = results;
    }
}


