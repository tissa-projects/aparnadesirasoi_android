package com.aparna.restaurantapp.models;

import org.json.JSONObject;

public class CategoryMenuModel {
    private  JSONObject jsonObject;
    private int position;
    private int viewType;

    public CategoryMenuModel(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
    public CategoryMenuModel(JSONObject jsonObject,int viewType) {
        this.jsonObject = jsonObject;
        this.viewType = viewType;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "CategoryMenuModel{" +
                "jsonObject=" + jsonObject.toString() +
                ", position=" + position +
                ", viewType=" + viewType +
                '}';
    }
}
