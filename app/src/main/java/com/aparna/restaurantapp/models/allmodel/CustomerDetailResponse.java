package com.aparna.restaurantapp.models.allmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerDetailResponse {
    @SerializedName("customer")
    @Expose
    private Customeresponse customer;
    @SerializedName("last_access")
    @Expose
    private String lastAccess;
    @SerializedName("extra")
    @Expose
    private Object extra;
    @SerializedName("salutation")
    @Expose
    private String salutation;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    public Customeresponse getCustomer() {
        return customer;
    }

    public void setCustomer(Customeresponse customer) {
        this.customer = customer;
    }

    public String getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(String lastAccess) {
        this.lastAccess = lastAccess;
    }

    public Object getExtra() {
        return extra;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
