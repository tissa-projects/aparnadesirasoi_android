package com.aparna.restaurantapp.models;

import org.json.JSONObject;

public class SelectedIngredentModel {
    private JSONObject jsonObject;
    private boolean isChecked;
    private String qty;
    private double price;

    public SelectedIngredentModel(JSONObject jsonObject, boolean isChecked,String qty,double price) {
        this.jsonObject = jsonObject;
        this.isChecked = isChecked;
        this.qty = qty;
        this.price = price;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
