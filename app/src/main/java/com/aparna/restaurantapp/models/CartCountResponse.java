package com.aparna.restaurantapp.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartCountResponse {

    @SerializedName("cartItemCount")
    @Expose
    private JsonObject cartItemCount;

    public JsonObject  getCartItemCount() {
        return cartItemCount;
    }

    public void setCartItemCount(JsonObject  cartItemCount) {
        this.cartItemCount = cartItemCount;
    }
}
