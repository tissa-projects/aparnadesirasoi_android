package com.aparna.restaurantapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartCountRequest {

    @SerializedName("errors")
    @Expose
    private List<Error> errors = null;
    @SerializedName("data")
    @Expose
    private CartCountResponse data;

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public CartCountResponse getData() {
        return data;
    }

    public void setData(CartCountResponse data) {
        this.data = data;
    }
}
