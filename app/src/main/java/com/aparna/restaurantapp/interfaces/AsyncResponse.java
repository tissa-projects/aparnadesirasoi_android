package com.aparna.restaurantapp.interfaces;

public interface AsyncResponse {
    void processResponse(String response);
}
