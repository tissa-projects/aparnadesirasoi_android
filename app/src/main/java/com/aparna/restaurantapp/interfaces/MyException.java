package com.aparna.restaurantapp.interfaces;

import android.content.Context;

import com.aparna.restaurantapp.Utilities.CustomDialogs;


public class MyException extends RuntimeException {

    public MyException(Context ctx) {
        super();
        CustomDialogs.dialogSessionExpire(ctx);
    }

}
